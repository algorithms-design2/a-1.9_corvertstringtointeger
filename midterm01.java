import java.util.Scanner;
import java.lang.Math;

public class midterm01 {
    public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        String s = ip.next();
        double answer = convertToInt(s);
        System.out.println(answer);
    }

    public static double convertToInt(String s) {
        double sum = 0;
        int n = s.length()-1;
        for (int i = 0; i < s.length(); i++) {
            String character = s.substring(i, i + 1);
            double num = checkNumber(character);
            sum = sum+(num*Math.pow(10, n));
            n--;
        }
        return sum;
    }

    public static int checkNumber(String number) {
        int num = 0;
        switch (number) {
            case "0":
                num = 0;
                break;
            case "1":
                num = 1;
                break;
            case "2":
                num = 2;
                break;
            case "3":
                num = 3;
                break;
            case "4":
                num = 4;
                break;
            case "5":
                num = 5;
                break;
            case "6":
                num = 6;
                break;
            case "7":
                num = 7;
                break;
            case "8":
                num = 8;
                break;
            case "9":
                num = 9;
                break;             
        }
        return num;
    }
}